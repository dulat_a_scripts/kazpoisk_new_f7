var subscribeToken = '';
//android
if(platformName == "android"){

  function getAndroidFcmPermit(){
    cordova.plugins.firebase.messaging.requestPermission().then(function() {
      console.log("Push messaging is allowed");
      getFcmToken();
    });
  }

  function getFcmToken(){
    cordova.plugins.firebase.messaging.getToken().then(function(token) {
      console.log("Got device android token: ", token);

      var emailS = getEmail();
      var data = {
        email:emailS,
        token:token
      }

      subscribeToken = token;

      socket.emit("setFirebaseToken",data);
      SubscribeToAndroidTopic();
    });
  }

  function SubscribeToAndroidTopic(){
    cordova.plugins.firebase.messaging.subscribe("2click");
  }

  cordova.plugins.firebase.messaging.onMessage(function(payload) {
    console.log("New foreground FCM message: ", payload);
  });

  cordova.plugins.firebase.messaging.onBackgroundMessage(function(payload) {
    console.log("New background FCM message: ", payload);
  });

  setTimeout(function(){
      getAndroidFcmPermit();
  },4000);



socket.on('setFirebaseToken', function(data){
              console.log(data);
        });



}


//android

if(platformName == "browser"){

  // Your web app's Firebase configuration

  var firebaseConfig = {
    apiKey: "AIzaSyCKw1-fhxaKPS4hLOdbWxqJp1FoX7esbFs",
    authDomain: "blissful-land-201717.firebaseapp.com",
    databaseURL: "https://blissful-land-201717.firebaseio.com",
    projectId: "blissful-land-201717",
    storageBucket: "blissful-land-201717.appspot.com",
    messagingSenderId: "791144738416",
    appId: "1:791144738416:web:07122e037fb7a4f982ceb5"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);



  const messaging = firebase.messaging();



function GetPermit(){
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');

      //notifPermit = 1;
      getNotifToken();


    }else {
      console.log('Unable to get permission to notify.');

    }
  });

}




function getNotifToken(){

      messaging.getToken().then((currentToken) => {
      if (currentToken) {
        console.log(currentToken);
        // localStorage.setItem("firebaseToken",currentToken);
        // localStorage.setItem("listenfirebaseToken","1");
        subscriptToTopic(currentToken);
        subscribeToken = currentToken;
        var emailS = getEmail();

        var data = {
          email:emailS,
          token:currentToken
        }

        socket.emit("setWebFirebaseToken",data);
        // sendTokenToServer(currentToken);
        // updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        // updateUIForPushPermissionRequired();
        // setTokenSentToServer(false);
      }
      }).catch((err) => {
          console.log('An error occurred while retrieving token. ', err);
      // showToken('Error retrieving Instance ID token. ', err);
      // setTokenSentToServer(false);
      });

      messaging.onTokenRefresh(() => {
        messaging.getToken().then((refreshedToken) => {
          console.log('Token refreshed.');
          // Indicate that the new Instance ID token has not yet been sent to the
          // app server.
          console.log(refreshedToken);
          subscribeToken = refreshedToken;
          var emailS = getEmail();

          var data = {
            email:emailS,
            token:refreshedToken
          }

          socket.emit("setWebFirebaseToken",data);
          // localStorage.setItem("firebaseToken",refreshedToken);
          // localStorage.setItem("listenfirebaseToken","1");
          subscriptToTopic(refreshedToken);
          // Send Instance ID token to app server.

          // ...
        }).catch((err) => {
          console.log('Unable to retrieve refreshed token ', err);

        });
      });

}

socket.on('setWebFirebaseToken', function(data){
              console.log(data);
        });

messaging.onMessage((payload) => {
  console.log('Message received. ', payload);
});




function subscriptToTopic(token){

  //localStorage.setItem("firebaseWebToken",token);
  var emailS = getEmail();

  var data = {
      email:emailS,
      token:token,
      topicName:"2click"
    }

  socket.emit("subscribeToTopic",data);
  //localStorage.setItem("listenfirebaseWebToken","1");
  //token need to send to server
}

socket.on('subscribeToTopic', function(data){
              console.log(data);
        });


setTimeout(function(){
GetPermit();
},4000);

}
