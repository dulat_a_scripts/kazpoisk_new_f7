var messagesF = myApp.messages.create({
  el: '.insert_com',
  scrollMessages:true,
  scrollMessagesOnEdge:true,
  newMessagesFirst:true,

  // First message rule
  firstMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if:
      - there is no previous message
      - or previous message type (send/received) is different
      - or previous message sender name is different
    */
    if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  lastMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if:
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
    if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  tailMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
      /* if (bascially same as lastMessageRule):
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
    if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
    return false;
  },

});


$(".leave_comment").click(function(){
    var vvAr = $(".tArea").val();

    // messagesF.addMessage({
    //   text: 'test message <i onclick="goShowMeRecords(1,2)" class="material-icons" >touch_app</i>',
    //   avatar:"https://cdn.framework7.io/placeholder/people-100x100-9.jpg",
    //   name:"testName"
    // });

    //return false;

    var vArea = cleanxss(vvAr);

    var ident = checkIdentity();

    if(ident == false){
      return false;
    }

    if(vArea.length > 2){
      console.log(vArea);

      if(validatexss(vvAr)){
        //work code here

        var emailS = getEmail();

        if(emailS){

          var useremailN = localStorage.getItem("useremail");
          var avatarS = getImage();
          if(useremailN == 0){
             useremailN = 0;
          }
          var systemCommentsMail = "systemComments@gmail.com";
          var systemId = 111;

          var sendjs = {action:"setComments",email:emailS,toEmail:systemCommentsMail,id:systemId,text:vArea,avatar:avatarS,name:useremailN};
          console.log(sendjs);
          socket.emit('publicRequest', sendjs);

          $(".tArea").val("");
        }

        $(".tArea").css("border","2px solid #5ac8fa");

        sendR();



      }else{
        $(".tArea").css("border","2px solid red");
      }


    }else{
      $(".tArea").css("border","2px solid red");
    }



});

$(".follow_comment").click(function(){

    var ident = checkIdentity();

    if(ident == false){
      return false;
    }

    var emailS = getEmail();

    var systemFollowMail = "systemComments@gmail.com";
    var systemFollowId = 111;

    socket.emit('userDetailService', {action:"followComment",email:emailS,remail:systemFollowMail,id:systemFollowId});

    if(subscribeToken != ''){
      var data = {
          email:emailS,
          token:subscribeToken,
          topicName:"followComment"
        }

      socket.emit("subscribeToTopic",data);
    }


    //var sendjs = {action:"setComments",email:emailS,toEmail:systemCommentsMail,id:systemId,text:vArea,avatar:avatarS,name:useremailN};
    //console.log(sendjs);
    //socket.emit('publicRequest', sendjs);

});

function checkfollowComment(){

  var emailS = getEmail();

  var systemFollowMail = "systemComments@gmail.com";
  var systemFollowId = 111;

  socket.emit('userDetailService', {action:"checkfollowComment",email:emailS,remail:systemFollowMail,id:systemFollowId});

}



socket.on('publicRequest', function(data){

              //console.log(data);

              if(data.action == "checkComments"){

                //$(".insert_com").empty();
                messagesF.clear();

                var messageArray = new Array();


                for(var i = data.data.length - 1;i > -1;i--){


                  var object = {
                    text: '<div style="cursor:pointer;" onclick="goShowMeRecords(' + data.data[i].id + ',\'' + data.data[i].com_email + '\')">' + data.data[i].com_text + '<i class="material-icons" >touch_app</i></div>',
                    header:data.data[i].com_date,
                    name: data.data[i].name,
                    avatar: data.data[i].image_url,
                    type: 'received',
                    attrs: {
                            'id': data.data[i].id,
                            'author-email': data.data[i].com_email
                          }
                  };

                  messageArray.push(object);
                  //$(".insert_com").append(newShab);

                }


                messagesF.addMessages(messageArray);


              }


        });

        function sendR(){
          var emailS = getEmail();

          var systemCommentsMail = "systemComments@gmail.com";
          var systemId = 111;

            socket.emit('publicRequest', {action:"checkComments",email:emailS,toEmail:systemCommentsMail,id:systemId});
        }

        setTimeout(function(){

            sendR();
            checkfollowComment();

        },2000);

        function goShowMeRecords(a,b){
          //console.log(a);
          //console.log(b);
          localStorage.setItem("commentId",a);
          localStorage.setItem("commentemail",b);

          setTimeout(function(){
              sendComments();
          },500)

        }
