var messagesL = myApp.messages.create({
  el: '.load_comments',
  scrollMessages:true,
  scrollMessagesOnEdge:true,
  newMessagesFirst:true,

  // First message rule
  firstMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if:
      - there is no previous message
      - or previous message type (send/received) is different
      - or previous message sender name is different
    */
    if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  lastMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if:
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
    if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  tailMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
      /* if (bascially same as lastMessageRule):
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
    if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
    return false;
  },

});

var messagebar = myApp.messagebar.create({
  el: '.messagebarX',
  scrollMessages:true,
  scrollMessagesOnEdge:true
});


// Send Message
$$('.send-answer').on('click', function () {

  var bbar = messagebar.getValue().replace(/\n/g, '<br>').trim();
  var text = cleanxss(bbar);

  var ident = checkIdentity();

  if(ident == false){
    return false;
  }
  // return if empty message
  if (!text.length) return;

  if(validatexss(text)){
    //work code here

    var emailS = getEmail();

    if(emailS){

      var avatarC = getImage();

      var commentemail = localStorage.getItem("commentemail");
      var commentId = localStorage.getItem("commentId");

      if(!commentId){
        myApp.dialog.alert('Необходимо выбрать заявку','system');
        return false;
      }

      if(emailS == commentemail){
        sendNotification("Not for Yourself!");
        return false;
      }

      var sendjsx = {action:"setComments",email:emailS,toEmail:commentemail,id:commentId,text:text,avatar:avatarC,name:emailS};

      //console.log(sendjsx);

      socket.emit('publicSubcomments', sendjsx);
      socket.emit('serviceMessages', {action:"setMessages",email:emailS,toEmail:commentemail,id:commentId,text:text,avatar:avatarC,name:emailS});

      messagebar.clear();
      messagebar.focus();

      sendComments();
      // Add message to messages
      // messagesL.addMessage({
      //   text: text,
      //   avatar:avatarC,
      //   name:emailS,
      // });
    }

    //sendR();

  }

});

socket.on('publicSubcomments', function(data){

              //console.log(data);

              if(data.action == "checkComments"){

                //$(".insert_com").empty();
                messagesL.clear();

                var messageArrays = new Array();

                for(var i = data.data.length - 1;i > -1;i--){

                  var object = {
                    text: data.data[i].com_text,
                    header:data.data[i].com_date,
                //    name: data.data[i].name,
                    avatar: data.data[i].image_url,
                    attrs: {
                            'id': data.data[i].id,
                            'author-email': data.data[i].com_email
                          }
                  };

                  messageArrays.push(object);
                  //$(".insert_com").append(newShab);

                }


                messagesL.addMessages(messageArrays);


              }


        });

        function sendComments(){
          var emailS = getEmail();

          messagesL.clear();
          var commentemail = localStorage.getItem("commentemail");
          var commentId = localStorage.getItem("commentId");

            socket.emit('publicSubcomments', {action:"checkComments",email:emailS,toEmail:commentemail,id:commentId});
        }
