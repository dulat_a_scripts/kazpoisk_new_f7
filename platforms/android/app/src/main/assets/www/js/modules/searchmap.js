var mapg;
var features = new Array();
var markerCleanArray = new Array();
var tempMapData = new Array();


function initMapz(data) {

  if(!data){
    return false;
  }

  for(var h = 0;h < markerCleanArray.length;h++){
    markerCleanArray[h].setMap(null);
  }

  markerCleanArray = new Array();


  //search central points
      var centralLat = localStorage.getItem("lat");
      var centrallng = localStorage.getItem("lng");

      var FixsavecentralPoint = 0;

      if(centralLat && centrallng){

          FixsavecentralPoint = 1;

           mapg = new google.maps.Map(
              document.getElementById('markermap'),{
                center: new google.maps.LatLng(JSON.parse(centralLat), JSON.parse(centrallng)),
                zoom: 11});

      }

  //search central points

  features = [];
  var centralPointfix = 0;
  var city = $(".searchregion").val();

  tempMapData = data;

  for(var kkk = 0;kkk < data.length;kkk++){

      if((data[kkk].Latitude != null) && (data[kkk].Longitude != null)){

        var baseUrlmap = localStorage.getItem("baseurlimg2");

        var priceLength = String(data[kkk].sena).length;

        var image = {
            url: 'https://myserverifind.hopto.org/assets/entry/uploads/icon.png',
            // This marker is 35 pixels wide by 35 pixels high.
            size: new google.maps.Size(50, 20),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 20)
          };

          var coordObj = {
            lat:data[kkk].Latitude,
            lng:data[kkk].Longitude,
            sena:data[kkk].sena,
            image:image,
            len:priceLength,
            title:data[kkk].zagolovok,
            photo:baseUrlmap + CheckPhoto(data[kkk]),
            opisanie:data[kkk].opisanie,
            id:data[kkk].id
          }

          features.push(coordObj);

          if((centralPointfix == 0) && (data[kkk].city == city) && (FixsavecentralPoint == 0)){

          mapg = new google.maps.Map(
              document.getElementById('markermap'),
              {center: new google.maps.LatLng(data[kkk].Latitude,data[kkk].Longitude), zoom: 11});

              centralPointfix = 1;

          }


      }


  }


  //search coordinates

  // Create markers.



  for (var i = 0; i < features.length; i++) {

      //popup
      //markers
      var lab;
      if(features[i].len > 4){
         lab = {
          text: String(features[i].sena),
          color: 'white',
          fontSize: '8px',
          fontWeight: 'normal'
        }
      }else{
         lab = {
          text: String(features[i].sena),
          color: 'white',
          fontSize: '12px',
          fontWeight: 'normal'
        }
      }

    //  console.log(features[i].item);



      var marker = new google.maps.Marker({
          position: {lat: features[i].lat, lng: features[i].lng},
          map: mapg,
          icon: features[i].image,
          label: lab
        });

        markerCleanArray.push(marker);

      var infowindow = new google.maps.InfoWindow();

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              //infowindow.setContent(location[i][0]);
              var contentString = '<div id="content viewcontent" onclick="viewContent('+ features[i].id + ')">'+
                '<div class="union"><img class="fl_left" src="'+ features[i].photo +
                '" ></div><div class="fl_right"><div class="fl_right_text">'+
                features[i].title +'</div><div class="fl_right_price">'+ features[i].sena +
                '</div><div class="below_text">' + features[i].opisanie +'</div></div>'+
                '</div>';



              infowindow.setContent(contentString);
              infowindow.open(mapg, marker);
          }
      })(marker, i));



  };
}

function viewContent(item){
      //tempMapData
      for(var j = 0;j < tempMapData.length;j++){
        if(tempMapData[j].id == item){
          navigateToDetailMap(tempMapData[j]);
          return false;
        }
      }


}

function navigateToDetailMap(item) {

  var check = checkAuthorize();

  if(check == true){
    senditem = item;
    router.navigate({ name: 'n1'});

  }else{
    router.navigate({ name: 'login' });
  }

}
