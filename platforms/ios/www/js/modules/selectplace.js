function initAutocomplete() {


  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: CurrentLatitude, lng: CurrentLongitude},
    zoom: 13,
    mapTypeControl:false,
    fullscreenControl:false,
    mapTypeId: 'roadmap'
  });

  // Create the search box and link it to the UI element.
  var inputx = document.getElementById('pac-input');
  var searchBoxx = new google.maps.places.SearchBox(inputx);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputx);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBoxx.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBoxx.addListener('places_changed', function() {

    var places = searchBoxx.getPlaces();



    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      //console.log(place.name);
      $(".choosemap").val(place.name);
      $(".searchregion").val(place.name);
      $(".successchooseregion").show();
      localStorage.setItem("place",place.name);


      localStorage.setItem("point",JSON.stringify(place.geometry.location));
      localStorage.setItem("lat",JSON.stringify(place.geometry.location.lat()));
      localStorage.setItem("lng",JSON.stringify(place.geometry.location.lng()));

      //console.log(place);
      CurrentLatitude = place.geometry.location.lat();
      CurrentLongitude = place.geometry.location.lng();
      //console.log(CurrentLatitude);
      //console.log(CurrentLongitude);

      point = place.geometry.location;

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });


      // if (navigator.userAgent.match(/(iPad|iPhone|iPod|android)/g)) {
      //     setTimeout(function() {
      //         var container = document.getElementsByClassName('pac-container')[0];
      //         container.addEventListener('touchend', function(e) {
      //             e.stopImmediatePropagation();
      //         });
      //     }, 500);
      // }

      // $("input").focusin(function () {
      //     $(document).keypress(function (e) {
      //         if (e.which == 13) {
      //             var firstResult = $(".pac-container .pac-item:first").text();
      //
      //             $(".choosemap").val(firstResult);
      //             $(".searchregion").val(firstResult);
      //             $("#pac-input").val(firstResult);
      //
      //
      //         } else {
      //             $(".pac-container").css("visibility","visible");
      //         }
      //
      //     });
      // });
}
