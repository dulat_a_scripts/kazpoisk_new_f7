//android
if(platformName == "android"){

FCMPlugin.onTokenRefresh(function(token){
    console.log( token );

    var emailS = getEmail();
    var data = {
      email:emailS,
      token:token
    }

    socket.emit("setFirebaseToken",data);
});

FCMPlugin.getToken(function(token){
  var emailS = getEmail();
  var data = {
    email:emailS,
    token:token
  }
  socket.emit("setFirebaseToken",data);
    console.log(token);
});

FCMPlugin.subscribeToTopic('2click');

socket.on('setFirebaseToken', function(data){
              console.log(data);
        });


FCMPlugin.onNotification(function(data){
    if(data.wasTapped){
      //Notification was received on device tray and tapped by the user.
      console.log( JSON.stringify(data) );
    }else{
      //Notification was received in foreground. Maybe the user needs to be notified.
      console.log( JSON.stringify(data) );
    }
});

}


//android

if(platformName == "browser"){

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyA7YPJeO7d8wsZjkQ10R0yRE5AKvoBLvU0",
    authDomain: "click-f1304.firebaseapp.com",
    databaseURL: "https://click-f1304.firebaseio.com",
    projectId: "click-f1304",
    storageBucket: "click-f1304.appspot.com",
    messagingSenderId: "389082948006",
    appId: "1:389082948006:web:32f1a07bc1f0fdd0"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);



  const messaging = firebase.messaging();



function GetPermit(){
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');

      //notifPermit = 1;
      getNotifToken();


    }else {
      console.log('Unable to get permission to notify.');

    }
  });

}




function getNotifToken(){

      messaging.getToken().then((currentToken) => {
      if (currentToken) {
        console.log(currentToken);
        // localStorage.setItem("firebaseToken",currentToken);
        // localStorage.setItem("listenfirebaseToken","1");
        subscriptToTopic(currentToken);
        var emailS = getEmail();

        var data = {
          email:emailS,
          token:currentToken
        }

        socket.emit("setWebFirebaseToken",data);
        // sendTokenToServer(currentToken);
        // updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        // updateUIForPushPermissionRequired();
        // setTokenSentToServer(false);
      }
      }).catch((err) => {
          console.log('An error occurred while retrieving token. ', err);
      // showToken('Error retrieving Instance ID token. ', err);
      // setTokenSentToServer(false);
      });

      messaging.onTokenRefresh(() => {
        messaging.getToken().then((refreshedToken) => {
          console.log('Token refreshed.');
          // Indicate that the new Instance ID token has not yet been sent to the
          // app server.
          console.log(refreshedToken);
          var emailS = getEmail();

          var data = {
            email:emailS,
            token:refreshedToken
          }

          socket.emit("setWebFirebaseToken",data);
          // localStorage.setItem("firebaseToken",refreshedToken);
          // localStorage.setItem("listenfirebaseToken","1");
          subscriptToTopic(refreshedToken);
          // Send Instance ID token to app server.

          // ...
        }).catch((err) => {
          console.log('Unable to retrieve refreshed token ', err);

        });
      });

}

socket.on('setWebFirebaseToken', function(data){
              console.log(data);
        });

messaging.onMessage((payload) => {
  console.log('Message received. ', payload);
});




function subscriptToTopic(token){

  //localStorage.setItem("firebaseWebToken",token);
  var emailS = getEmail();

  var data = {
      email:emailS,
      token:token,
      topicName:"2click"
    }

  socket.emit("subscribeToTopic",data);
  //localStorage.setItem("listenfirebaseWebToken","1");
  //token need to send to server
}

socket.on('subscribeToTopic', function(data){
              console.log(data);
        });


setTimeout(function(){
GetPermit();
},4000);

}
